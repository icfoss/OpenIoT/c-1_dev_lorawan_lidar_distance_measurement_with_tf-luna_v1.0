## c-1_dev_LoRaWAN_LiDAR_distance_measurement_with_TF-luna_v1.0
---

![image-1](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_lidar_distance_measurement_with_tf-luna_v1.0/-/raw/main/Images/image-1.png?ref_type=heads)

### _Overview_
-----
The c-1_dev_LoRaWAN_LiDAR_distance_measurement_with_TF-luna_v1.0 project builds a system for remote distance measurement using LiDAR technology and LoRaWAN communication. This system can be used for various applications which requires measuring the distance between two points (upto 8 metre),This is accomplished by using a TF-Luna sensor to measure the distance between the sensor and a target object. LiDAR technology ensures high accuracy, while LoRaWAN enables long-range data transmission for remote monitoring. The power efficiency of the system is achieved by using c-1_dev board which is based on the ST's STM32L072ZY3TR microcontroller, allowing for extended operation with minimal maintenance and low power consumption.

### _Features_
---
- LoRaWAN compatible
- Distance measurement accuracy ±6cm@ (0.2-3m) and ±2%@ (3m-8m)

### _Recorded Parameters_
---
- Distance in cm
- Battery voltage

### _Pre-Requisite_
---
- [STM32 CUBE IDE](https://www.st.com/en/development-tools/stm32cubeide.html)
- [C-1_Dev v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0)
- [TF-Luna](https://robu.in/product/tf-luna-lidar-distance-sensor/)
- [2N7000 MOSFET](https://in.element14.com/on-semiconductor/2n7000/mosfet-n-channel-200ma-60v-to/dp/9845178?pf_custSiteRedirect=true)

### _Getting Started_
---
- connect the components as shown in the wiring diagram [here](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_lidar_distance_measurement_with_tf-luna_v1.0/-/tree/main/Hardware/wiring_diagram?ref_type=heads)
- clone the repository 
 ``` sh 
 git clone https://gitlab.com/abdularshadvs/c-1_dev_lorawan_lidar_distance_measurement_with_tf-luna_v1.0.git
 ```
 - open STM32 cube ide and create a workspace
 - Import the TF-Luna folder from the cloned repository into the workspace

 ![image-2](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_lidar_distance_measurement_with_tf-luna_v1.0/-/raw/main/Images/image-2.png?ref_type=heads)

 - Configure the keys and address in se-identity.h
```c
// end-device IEEE EUI (big endian)
#define LORAWAN_DEVICE_EUI                  { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

// App/Join server IEEE EUI (big endian)
#define LORAWAN_JOIN_EUI                    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

// Set the device address on the network (big endian)
#define LORAWAN_DEVICE_ADDRESS              ( uint32_t )0x00000000
```
***NOTE***
- *For OTA devices, set the APP root key and leave others blank*
- *For ABP devices, set the Network root key,Network session key and Application session key*
- *For LoRaWAN version 1.0.x use same key for Network root  key and Network session key*

```c
// Application root key
#define LORAWAN_APP_KEY                     00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00

// Network root key
#define LORAWAN_NWK_KEY                     00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
/*!
 * Defines the application data transmission duty cycle. 10s, value in [ms].
 */
#define APP_TX_DUTYCYCLE                           10000

// Forwarding Network session key
#define LORAWAN_NWK_S_KEY                   00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00

// Application session key
#define LORAWAN_APP_S_KEY                   00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
```
- Set the Transmission Interval and Activation type (ABP/OTA) in lora_app.h
```c
// Defines the application data transmission duty cycle. 10s, value in [ms].
#define APP_TX_DUTYCYCLE                    10000
```
- save changes and build the project
- connect the c-1 dev to the st-link as shown in the programmer connection diagram [here](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_lidar_distance_measurement_with_tf-luna_v1.0/-/tree/main/Hardware/programmer_connection_diagram?ref_type=heads)
- click the debug option inside the stm32 cube ide to upload the program.

### _Acknowledgement_
---
- The project is built upon the base code of ST's I-CUBE-LRWAN package [here](https://www.st.com/en/embedded-software/i-cube-lrwan.html)
- TF-Luna driver : forked and modified from [osos11](https://github.com/osos11-Git/STM32F401_TF_LUNA_I2C_HAL)
### _License_
---
This project is licensed under the MIT License - see the LICENSE.md file for details
